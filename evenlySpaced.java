public boolean evenlySpaced(int a, int b, int c) {
  int[] anArray = {a, b, c};
  Arrays.sort(anArray);

  int dLow = anArray[1] - anArray[0];
  int dHi = anArray[2] - anArray[1];

  if (dLow == dHi)
    return true;
  else
    return false;  
}